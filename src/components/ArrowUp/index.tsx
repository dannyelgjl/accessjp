import React from "react";

// bootstrap
import Nav from "react-bootstrap/Nav";

// icon
import ArrowDropUp from "@material-ui/icons/ArrowDropUp";

import "./styles.scss";

const Apresentation: React.FC = () => {
  const scrollHome = () => window.scrollTo(0, 0);

  return (
    <div className="container-nav right">
      <Nav.Item>
        <Nav.Link onClick={scrollHome}>
          <ArrowDropUp
            className="arrow"
            fontSize="large"
            style={{ marginRight: 10 }}
          />
        </Nav.Link>
      </Nav.Item>
    </div>
  );
};

export default Apresentation;
