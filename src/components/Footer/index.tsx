import React from "react";

// bootstrap
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// img
import imgFooter from "../../images/svg/lighthouse.svg";

import "./styles.scss";

const Footer: React.FC = () => (
  <Container fluid className="footer-master">
    <Row className="footer">
      <Col xs={1} sm={1} md={1} lg={1} />
      <Col xs={3} sm={3} md={3} lg={3} className="center line-up">
        <Row>
          <img
            src={imgFooter}
            alt="Logo lighthouse"
            className="logo-lighthouse"
          />
          <p>ACCESSJP © 2020 Daniel Jerônimo</p>
        </Row>
      </Col>
      <Col xs={3} sm={3} md={3} lg={3} />

      <Col xs={5} sm={5} md={5} lg={5} className="center">
        <p className="secondary">
          Todos os direitos reservados. Contato: (83) 99830-1966.
        </p>
      </Col>
    </Row>
  </Container>
);

export default Footer;
