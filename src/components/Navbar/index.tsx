import React, { useState, useEffect } from "react";

// bootstrap
import Nav from "react-bootstrap/Nav";
import NavBar from "react-bootstrap/Navbar";

// components
import ModalLogin from "../ModalLogin";

// redux
import { useSelector, RootStateOrAny, useDispatch } from "react-redux";
import { removeUser } from "../../store/ducks/user/actions";

// icons
import PersonPinCircle from "@material-ui/icons/PersonPinCircle";

// img
import lighthouse from "../../images/svg/lighthouse.svg";

import "./styles.scss";

interface User {
  id: string;
  token: string;
  name: string;
  email: string;
  password: string;
}

const Navbar: React.FC = () => {
  const user: User = useSelector((state: RootStateOrAny) => state.user.user);
  const dispatch = useDispatch();
  const [modalShow, setModalShow] = useState(false);

  const scrollZone = () => window.scrollTo(0, 1690);
  const scrollHome = () => window.scrollTo(0, 0);
  const scrollDetail = () => window.scrollTo(0, 3000);
  const scrollInfor = () => window.scrollTo(0, 750);

  const signOut = () => {
    dispatch(removeUser());
  };

  return (
    <>
      <NavBar className="director" fixed="top">
        <Nav className="nav">
          <div className="container-nav left">
            <div className="container-logo">
              <img src={lighthouse} alt="logo Map" className="logo-map" />
              <h3>AccessJp</h3>
            </div>
            <Nav.Item>
              <Nav.Link onClick={scrollHome}>Home</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link onClick={scrollZone}>Zonas</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="link-1" onClick={scrollDetail}>
                Sobre
              </Nav.Link>
            </Nav.Item>
          </div>
          <div className="container-nav right">
            <Nav.Item>
              <Nav.Link
                onClick={() => {
                  user.name === "" ? setModalShow(true) : signOut();
                }}
              >
                <PersonPinCircle fontSize="default" />
                {user.token !== "" ? user.name : "Entrar"}
              </Nav.Link>
            </Nav.Item>
          </div>
        </Nav>
      </NavBar>
      <ModalLogin show={modalShow} onHide={() => setModalShow(false)} />
      <main>
        <section className="main">
          <div className="container">
            <div className="main-message">
              <h3>Venha Conhecer</h3>
              <h1>ACCESSJP</h1>
              <div className="cta">
                <a onClick={scrollInfor} id="btn">
                  Click Aqui
                </a>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
};

export default Navbar;
