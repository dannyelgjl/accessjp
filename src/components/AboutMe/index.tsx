import React from "react";

// bootstrap
import Nav from "react-bootstrap/Nav";

// icons
import GitHub from "@material-ui/icons/GitHub";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import InstagramIcon from "@material-ui/icons/Instagram";

// img
import myImg from "../../images/avatar.jpg";

import "./styles.scss";

const MyInfor: React.FC = () => {
  return (
    <main>
      <section className="my-infors">
        <div className="container">
          <div className="my-infor">
            <div className="my-infor-customer">
              <img id="my-img" src={myImg} />
              <div className="my-infor-text-box">
                <h1>Daniel G.</h1>
                <p>Software Develop</p>
                <p>Graduando em Ciências da Computação pela Unipê</p>

                <div className="container-icon center">
                  <Nav.Item>
                    <Nav.Link
                      target="_blank"
                      href="https://github.com/dannyelgjl"
                    >
                      <GitHub fontSize="large" />
                    </Nav.Link>
                  </Nav.Item>

                  <Nav.Item>
                    <Nav.Link
                      target="_blank"
                      href="https://www.linkedin.com/in/danielgjl/"
                      onClick={() => {}}
                    >
                      <LinkedInIcon fontSize="large" />
                    </Nav.Link>
                  </Nav.Item>

                  <Nav.Item>
                    <Nav.Link
                      target="_blank"
                      href="https://www.instagram.com/danieljeronimo_/"
                      onClick={() => {}}
                    >
                      <InstagramIcon fontSize="large" />
                    </Nav.Link>
                  </Nav.Item>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
};

export default MyInfor;
