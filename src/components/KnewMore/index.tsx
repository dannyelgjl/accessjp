import React from "react";

// imgs
import jpImg from "../../images/jp.jpg";
import logoMap from "../../images/svg/logoMap.svg";

import "./styles.scss";

const PoemOCaboBranco: React.FC = () => {
  return (
    <>
      <div className="content">
        <div className="content-left">
          <img src={jpImg} alt="" />
        </div>
        <div className="content-right">
          <div className="information">
            <h2>Como funciona?</h2>
            <p>
              Aqui você pode encontrar seu bairro e deixar seu feedback.
              Selecione a zona desejada e encontre o seu bairro, basta passar o
              mouse sobre a zona que irá aparecer todos os bairros refente a
              zona selecionada.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default PoemOCaboBranco;
