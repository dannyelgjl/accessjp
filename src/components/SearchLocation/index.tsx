import React, { useEffect, useState } from "react";

// bootstrap
import Form from "react-bootstrap/Form";
import ButtonBootstrap from "react-bootstrap/Button";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";
import Spinner from "react-bootstrap/Spinner";

// components
import ModalLogin from "../ModalLogin";
import ModalComment from "../../components/ModalComment";
import ModalMessageSend from "../../components/ModalMessageSend";

// leaflet
import { Map, TileLayer, Marker } from "react-leaflet";
import { Select, MenuItem, FormControl, Button } from "@material-ui/core";

// data
import dataJSON from "../../data/neighborhoods.json";

// icons
import { Search } from "@material-ui/icons";

// imgs
import arrow from "../../images/gif/arrow.gif";
import map from "../../images/map.svg";
import loading from "../../images/svg/loading.svg";

import "./styles.scss";

interface Neighborhoods {
  zone: number;
  neighborhood: any;
}

interface Neighborhood {
  id: number;
  name: string;
  location: [number, number];
}

const SearchLocation: React.FC = () => {
  const data = dataJSON.neighborhoods;

  const [modalShow, setModalShow] = useState<boolean>(false);

  const [initialPosition, setInitialPosition] = useState<[number, number]>([
    -7.1402162,
    -34.8881228,
  ]);
  const [neighborhoods, setNeighborhoods] = useState<Neighborhood[]>([]);
  const [zoneSelected, setZoneSelected] = useState(0);

  const [neighborhoodSelected, setNeighborhoodSelected] = useState<
    Neighborhood
  >({ name: "", id: 0, location: [0, 0] });

  const [showModalSuccess, setShowModalSuccess] = useState<boolean>(false);
  const [showModalLogin, setShowModalLogin] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");

  const searchZone = () => {
    data.map((item: Neighborhoods) => {
      if (item.zone === zoneSelected) {
        setNeighborhoods(item.neighborhood);
      } else if (0 === zoneSelected) {
        setNeighborhoods([]);
      }
    });
  };
  //Sempre que houver alteração na zona a função a cima será executada
  useEffect(() => {
    searchZone();
  }, [zoneSelected]);

  const openModalSendMessage = (neighborhood: Neighborhood) => {
    setModalShow(true);
    setNeighborhoodSelected(neighborhood);
  };

  return (
    <div className="container-searchLocation">
      <div className="title-search">
        <img src={map} alt="map" />
        <h4>Selecione uma região e localize um bairro abaixo</h4>
      </div>
      <img className="arrow-gif" src={arrow} alt="arrow" />

      <Form className="form-search">
        <FormControl variant="outlined" size="small" style={{ width: "400px" }}>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            onChange={(e: any) => setZoneSelected(Number(e.target.value))}
            value={zoneSelected}
          >
            <MenuItem value={0}>
              <em>Escolha uma zona</em>
            </MenuItem>
            {data.map((item: Neighborhoods) => (
              <MenuItem key={item.zone} value={item.zone}>
                <OverlayTrigger
                  trigger="hover"
                  placement="right-start"
                  overlay={
                    <Popover id="popover-basic">
                      {item.neighborhood.map((text: Neighborhood) => (
                        <Popover.Content key={text.id}>
                          {text.name}
                        </Popover.Content>
                      ))}
                    </Popover>
                  }
                >
                  <div
                    style={{
                      width: "100%",
                      fontFamily: '"Nunito", sans-serif',
                    }}
                  >
                    {`Zona ${item.zone}`}
                  </div>
                </OverlayTrigger>
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <ButtonBootstrap className="primary-button">
          <Search />
        </ButtonBootstrap>
      </Form>

      <div className="wrapper-neighborhood">
        <div className="container-map">
          <Map center={initialPosition} zoom={12}>
            <TileLayer
              attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {neighborhoods.map((mark: Neighborhood) => (
              <Marker
                position={mark.location}
                title={mark.name}
                key={mark.id}
              />
            ))}
          </Map>
        </div>
        <div className="container-info">
          {!neighborhoods.length && (
            <>
              <img src={loading} alt="loading" />
              <span>
                Você ainda não escolheu sua zona
                <Spinner
                  className="Spinner"
                  animation="grow"
                  style={{ marginRight: 7 }}
                />
                <Spinner
                  className="Spinner"
                  style={{ marginRight: 7 }}
                  animation="grow"
                />
                <Spinner className="Spinner" animation="grow" />
              </span>
            </>
          )}
          {!!neighborhoods.length && (
            <div className="container-choiceNeighborhood">
              <h1>Zona {zoneSelected} selecionada</h1>

              <p>Selecione um Bairro abaixo para deixar um comentário:</p>

              <div className="container-neighborhoods">
                {neighborhoods.map((neighborhood: Neighborhood) => (
                  <div className="wrapper-button" key={neighborhood.id}>
                    <ButtonBootstrap
                      onClick={() => openModalSendMessage(neighborhood)}
                      id="btn-zone"
                      className="primary-button"
                    >
                      {neighborhood.name}
                    </ButtonBootstrap>
                  </div>
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
      <ModalComment
        show={modalShow}
        onHide={() => setModalShow(false)}
        neighborhood={neighborhoodSelected.name as any}
        openLogin={() => setShowModalLogin(true)}
        sendMessage={(msg: string) => {
          setMessage(msg);
          setShowModalSuccess(true);
        }}
      />

      <ModalLogin
        show={showModalLogin}
        onHide={() => setShowModalLogin(false)}
      />

      <ModalMessageSend
        show={showModalSuccess}
        onHide={() => {
          setShowModalSuccess(false);
          setMessage("");
        }}
        info={{
          message,
          zone: zoneSelected,
          id: neighborhoodSelected.id,
          name: neighborhoodSelected.name,
        }}
      />
    </div>
  );
};

export default SearchLocation;
