import React, { useState, useEffect } from "react";

// bootstrap
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

// redux
import { useSelector, RootStateOrAny } from "react-redux";

// imgs
import error from "../../images/svg/error.svg";
import comment from "../../images/svg/comment.svg";

import "./styles.scss";

interface MyProps {
  show: boolean;
  onHide(): any;
  neighborhood: string;
  openLogin(): any;
  sendMessage: any;
}

interface User {
  id: string;
  token: string;
  name: string;
  email: string;
  password: string;
}

const ModalComment = ({
  show,
  onHide,
  neighborhood,
  openLogin,
  sendMessage,
}: MyProps) => {
  const props = { show, onHide };
  const user: User = useSelector((state: RootStateOrAny) => state.user.user);

  const [message, setMessage] = useState<string>("");

  const submitMessage = () => {
    if (message !== "") {
      props.onHide();
      sendMessage(message);
    }
  };
  console.log(message);

  return (
    <Modal {...props} aria-labelledby="contained-modal-title-vcenter" centered>
      {user.token !== "" ? (
        <>
          <Modal.Header closeButton>
            <h4>Bairro {neighborhood} selecionado</h4>
          </Modal.Header>
          <Modal.Body>
            <img src={comment} className="d-block w-100" alt="" />
            <Form>
              <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label>
                  Deixe aqui um comentário sobre o bairro {neighborhood}:
                </Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  value={message}
                  onChange={(event: any) => setMessage(event.target.value)}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={submitMessage} variant="outline-light">
              Enviar
            </Button>
            <Button onClick={props.onHide} variant="outline-light">
              Voltar
            </Button>
          </Modal.Footer>
        </>
      ) : (
        <div className="container-disconnected">
          <Modal.Header closeButton>
            <h3>Entre em sua conta e deixe um comentário...</h3>
          </Modal.Header>
          <div className="content-img">
            <img id="image-error" src={error} alt="error" />
          </div>
          <Modal.Footer>
            <Button onClick={openLogin} variant="outline-light">
              Entrar
            </Button>
            <Button onClick={props.onHide} variant="outline-light">
              Fechar
            </Button>
          </Modal.Footer>
        </div>
      )}
    </Modal>
  );
};

export default ModalComment;
