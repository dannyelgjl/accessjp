import React from "react";

// bootstrap
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

// img
import messageSuccess from "../../images/svg/messagesucess.svg";

import "./styles.scss";

interface MyProps {
  show: boolean;
  onHide: any;
  info: {
    zone: number;
    id: number;
    name: string;
    message: string;
  };
}

const ModalSuccess = ({ show, onHide, info }: MyProps) => {
  const props = { show, onHide };

  return (
    <Modal {...props} aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header>
        <img src={messageSuccess} alt="message success" />{" "}
        <h5>
          Você deixou um comentário para o Bairro: {info.name} - id: {info.id},
          Zona: {info.zone}.
        </h5>
      </Modal.Header>
      <Modal.Body>
        <div className="use-comment">
          <p>
            <strong>Comentário: </strong>
            {info.message}
          </p>
        </div>
      </Modal.Body>
      <Modal.Footer className="footer-modalMessage">
        <Button onClick={props.onHide} id="buttonClose" variant="outline-light">
          Fechar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ModalSuccess;
