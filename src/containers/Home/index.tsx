import React from "react";

import Navbar from "../../components/Navbar";
import KnewMore from "../../components/KnewMore";
import SearchLocation from "../../components/SearchLocation";

import AboutMe from "../../components/AboutMe";

import ArrowUp from "../../components/ArrowUp";

//import Parallax from "../../components/Parallax";

import Footer from "../../components/Footer";

const Home: React.FC = () => {
  return (
    <>
      <Navbar />

      <KnewMore />
      <SearchLocation />
      <ArrowUp />
      <AboutMe />
      <Footer />
    </>
  );
};

export default Home;
